[](# Change Log

## [Unreleased]

### Added
- Displays random beer section, with a button/link to display random beers and random non-alcoholic beers
- Displays a search form where users can search for beers by name(using a free text search) or `brewed_before` (date) 
- Show search results from the PunkAPI.

--- 

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
)