export const PUNK_API = 'https://api.punkapi.com/v2/';
export const PLACEHOLER_IMAGE_URL =
  'https://via.placeholder.com/400x680?text=BrewDogRecipes';
export const PLACEHOLER_THUMBNAIL_URL =
  'https://via.placeholder.com/80x120?text=BrewDogRecipes';

export const API_LIMIT = 25;

export const SEARCH_KEYS = {
  name: {
    TYPE: 'text',
    PARAM: 'beer_name',
    DISPLAY: 'Name',
  },
  brewed_before: {
    TYPE: 'date',
    PARAM: 'brewed_before',
    DISPLAY: 'Brewed Before',
  },
};

export const REGEX = /^[0-9A-Za-z\s]+$/;
