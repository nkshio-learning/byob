import PropTypes from 'prop-types';

export const Beer = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  tagline: PropTypes.string.isRequired,
  image_url: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
});

export const SearchType = PropTypes.shape({
  TYPE: PropTypes.string.isRequired,
  PARAM: PropTypes.string.isRequired,
  DISPLAY: PropTypes.string.isRequired,
});

export const SearchQuery = PropTypes.shape({
  type: SearchType,
  value: PropTypes.string,
  page: PropTypes.number.isRequired,
});
