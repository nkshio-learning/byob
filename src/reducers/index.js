import { combineReducers } from 'redux';

import random from './random';
import discover from './discover';
import search from './search';
import loading from './loading';

const brewDog = combineReducers({
  search,
  random,
  discover,
  loading,
});

export default brewDog;
