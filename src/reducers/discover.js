import { API_LIMIT } from '../lib/constants';

const initialState = {
  error: '',
  beers: [],
  query: {
    page: 1,
  },
  loading: false,
  noMoreBeers: false,
};

export default function discover(state = initialState, action) {
  switch (action.type) {
    case 'SEARCH_REQUEST':
      return {
        ...state,
        loading: true,
      };
    case 'SEARCH_SUCCESS':
      return {
        ...state,
        beers:
          action.query.page === 1
            ? action.response.result
            : [...state.beers, ...action.response.result],
        loading: false,
        query: action.query,
        noMoreBeers: action.response.result.length < API_LIMIT,
      };
    case 'SEARCH_FAILURE':
      return {
        ...state,
        loading: false,
        // We have action.response here, if needed
        error: 'Something went wrong.',
      };
    default:
      return state;
  }
}
