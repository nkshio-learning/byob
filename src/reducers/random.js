export default function random(state = { loading: false }, action) {
  switch (action.type) {
    case 'RANDOM_BEER_REQUEST':
      return {
        ...state,
        loading: true,
      };
    case 'RANDOM_BEER_SUCCESS':
      return {
        loading: false,
        beer: action.response.result,
      };
    case 'RANDOM_BEER_FAILURE':
      return {
        ...state,
        loading: false,
        // We have action.response here, if needed
        error: 'Something went wrong, here is the last beer you loaded.',
      };
    default:
      return state;
  }
}
