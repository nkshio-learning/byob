const initialState = {
  text: 'Loading',
  speed: 300,
};

const loading = (state = initialState, action) => {
  switch (action.type) {
    case 'LOADING_TEXT':
      return {
        ...state,
        text:
          state.text.split('.').length === 4
            ? state.text.slice(0, -3)
            : state.text + '.',
      };
    default:
      return state;
  }
};

export default loading;
