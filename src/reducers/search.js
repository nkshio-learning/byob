import Date from 'dayjs';
import { SEARCH_KEYS, REGEX } from '../lib/constants';

const initialState = {
  value: '',
  error: '',
  searchKey: SEARCH_KEYS[Object.keys(SEARCH_KEYS)[0]],
};

const validate = (searchKey, value) => {
  const { TYPE } = searchKey;

  if (TYPE === 'text' && !REGEX.test(value))
    return {
      result: false,
      error: 'Input accepts only letters, numbers, hyphens and spaces',
    };

  if (TYPE === 'date' && !Date(value).isValid())
    return {
      result: false,
      error: 'Please select a valid date',
    };

  return {
    result: true,
  };
};

const search = (state = initialState, action) => {
  let valueValidated = null;

  switch (action.type) {
    case 'HANDLE_KEY':
      return {
        value: '',
        error: '',
        searchKey: SEARCH_KEYS[action.response],
      };
    case 'HANDLE_VALUE':
      valueValidated = validate(state.searchKey, action.response);

      if (!valueValidated.result)
        return {
          ...state,
          value: '',
          error: valueValidated.error,
        };

      return {
        ...state,
        value: action.response,
      };

    default:
      return state;
  }
};

export default search;
