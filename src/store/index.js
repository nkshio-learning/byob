import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import throttle from 'lodash/throttle';
import brewDog from '../reducers';
import { loadState, saveState } from './localStorage';

const configureStore = () => {
  const middlewares = [thunk];
  if (process.env.NODE_ENV !== 'production') {
    middlewares.push(createLogger());
  }

  const persistedState = loadState();
  const store = createStore(
    brewDog,
    persistedState,
    applyMiddleware(...middlewares),
  );
  store.subscribe(
    throttle(() => {
      saveState({
        random: {
          beer: store.getState().random.beer,
        },
        discover: {
          beers: store.getState().discover.beers,
        },
      });
    }, 1000),
  );

  return store;
};

export default configureStore;
