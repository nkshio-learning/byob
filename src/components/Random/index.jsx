import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import styles from './random.scss';
import Beer from '../Beer';
import Button from '../Button';
import Loading from '../Loading';
import * as actions from '../../actions';

class Random extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleNonAlcoholic = this.handleNonAlcoholic.bind(this);
  }

  componentDidMount() {
    this.handleClick();
  }

  handleNonAlcoholic() {
    this.handleClick(null, true);
  }

  handleClick(_, isNonAlcoholic = false) {
    this.props.fetchBeer(isNonAlcoholic);
  }

  render() {
    const { beer, error, loading } = this.props;
    if (loading) {
      return <Loading />;
    }

    return (
      <div className={styles['random-beer']}>
        <Beer {...beer} isFullPage />
        <div className={styles['action-container']}>
          <Button type="button" text="Random Beer" onClick={this.handleClick} />
          <Button
            type="button"
            className="button-light"
            text="Non Alcoholic beer"
            onClick={this.handleNonAlcoholic}
          />
        </div>
        {error ? <div className={styles['error']}>{error}</div> : null}
      </div>
    );
  }
}

Random.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.string,
  beer: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    tagline: PropTypes.string,
    image_url: PropTypes.string,
    description: PropTypes.string,
  }).isRequired,
  fetchBeer: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  error: state.random.error,
  beer: state.random.beer || {},
  loading: state.random.loading,
});

export default connect(
  mapStateToProps,
  actions,
)(Random);
