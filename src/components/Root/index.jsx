import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import styles from './root.scss';
import Nav from '../Nav';
import Random from '../Random';
import Discover from '../Discover';

const Root = ({ store }) => (
  <Provider store={store}>
    <BrowserRouter>
      <div className={styles['hero']}>
        <section className={styles['container']}>
          <Nav />
          <Switch>
            <Route exact path="/" component={Random} />
            <Route exact path="/discover" component={Discover} />
            <Redirect path="*" to="/" />
          </Switch>
        </section>
      </div>
    </BrowserRouter>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.shape({}).isRequired,
};

export default hot(module)(Root);
