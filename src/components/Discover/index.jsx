import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import List from '../List';
import Search from '../Search';
import * as actions from '../../actions';
import { SearchQuery, Beer } from '../../lib/propShapes';

class Discover extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInfinite = this.handleInfinite.bind(this);
  }

  componentDidMount() {
    this.props.discover({ page: 1 });
  }

  handleSubmit(searchKey, value) {
    const page = 1;
    this.props.discover({ page, searchKey, value });
  }

  handleInfinite() {
    let { page, searchKey, value } = this.props.query;

    page += 1;
    this.props.discover({ page, searchKey, value });
  }

  render() {
    return (
      <div>
        <Search onSubmit={this.handleSubmit} />
        <List {...this.props} onInfinite={this.handleInfinite} />
      </div>
    );
  }
}

Discover.propTypes = {
  query: SearchQuery,
  loading: PropTypes.bool,
  beers: PropTypes.arrayOf(Beer).isRequired,

  discover: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  query: state.discover.query,
  loading: state.discover.loading,
  beers: state.discover.beers || [],
});

export default connect(
  mapStateToProps,
  actions,
)(Discover);
