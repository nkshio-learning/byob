import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import styles from './loading.scss';

class Loading extends React.Component {
  componentDidMount() {
    const { text } = this.props;

    this.interval = window.setInterval(() => {
      this.props.dispatch({
        response: text,
        type: 'LOADING_TEXT',
      });
    }, this.props.speed);
  }

  componentWillUnmount() {
    window.clearInterval(this.interval);
  }

  render() {
    const { text } = this.props;

    return <p className={styles['content']}>{text}</p>;
  }
}

Loading.propTypes = {
  text: PropTypes.string.isRequired,
  speed: PropTypes.number.isRequired,

  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  text: state.loading.text,
  speed: state.loading.speed,
});

export default connect(mapStateToProps)(Loading);
