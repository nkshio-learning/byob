import React from 'react';
import PropTypes from 'prop-types';

import styles from './beer.scss';
import {
  PLACEHOLER_IMAGE_URL,
  PLACEHOLER_THUMBNAIL_URL,
} from '../../lib/constants';

const Beer = props => {
  if (props.isFullPage) {
    return (
      <div className={styles['full-page']}>
        <div className={styles['title']}>{props.name}</div>
        <div className={styles['image']}>
          <img
            alt={props.tagline}
            src={props.image_url || PLACEHOLER_IMAGE_URL}
          />
          <div className={styles['tagline']}>{props.tagline}</div>
        </div>
        <div className={styles['body']}>{props.description}</div>
      </div>
    );
  }
  return (
    <div className={styles['card']} key={props.id}>
      <div className={styles['image']}>
        <img
          alt={props.tagline}
          src={props.image_url || PLACEHOLER_THUMBNAIL_URL}
        />
      </div>
      <div>
        <div className={styles['title']}>{props.name}</div>
        <div className={styles['body']}>{props.description}</div>
      </div>
    </div>
  );
};

Beer.propTypes = {
  isFullPage: PropTypes.bool,

  id: PropTypes.number,
  name: PropTypes.string,
  tagline: PropTypes.string,
  image_url: PropTypes.string,
  description: PropTypes.string,
};

export default Beer;
