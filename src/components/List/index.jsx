import React from 'react';
import PropTypes from 'prop-types';

import Beer from '../Beer';
import Loading from '../Loading';
import styles from './list.scss';

class List extends React.Component {
  constructor(props) {
    super(props);

    this.onInfinite = this.onInfinite.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onInfinite, false);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onInfinite, false);
  }

  onInfinite() {
    // Checks that the page has scrolled to the bottom
    if (
      window.innerHeight + document.documentElement.scrollTop ===
      document.documentElement.offsetHeight
    ) {
      this.props.onInfinite();
    }
  }

  render() {
    const { beers, loading } = this.props;

    if (!loading && !beers.length) {
      return <div className={styles['error']}>No results</div>;
    }

    return (
      <div className={styles['list']}>
        {beers.length
          ? beers.map(beer => <Beer key={beer.id} {...beer} />)
          : null}
        {loading ? <Loading /> : null}
      </div>
    );
  }
}

List.propTypes = {
  loading: PropTypes.bool,
  onInfinite: PropTypes.func,
  beers: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default List;
