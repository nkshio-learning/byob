import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Button from '../Button';
import styles from './search.scss';
import { SearchType } from '../../lib/propShapes';
import { SEARCH_KEYS } from '../../lib/constants';

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.handleType = this.handleType.bind(this);
    this.handleValue = this.handleValue.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleType(event) {
    const searchKey = event.target.value;

    this.props.dispatch({
      response: searchKey,
      type: 'HANDLE_KEY',
    });
  }

  handleValue(event) {
    const { value } = event.target;

    this.props.dispatch({
      response: value,
      type: 'HANDLE_VALUE',
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const { searchKey, value } = this.props;
    this.props.onSubmit(searchKey, value);
  }

  render() {
    const { searchKey, value, error } = this.props;

    return (
      <div>
        <form className={styles['search']}>
          <div className={styles['input-container']}>
            <input
              value={value}
              placeholder="Search ..."
              onChange={this.handleValue}
              className={error ? styles['invalid'] : ''}
              type={searchKey ? searchKey.TYPE : 'text'}
            />
            {error ? <div className={styles['error']}>{error}</div> : null}
          </div>
          <div className={styles['type-selector']}>
            {Object.keys(SEARCH_KEYS).map(el => (
              <label key={SEARCH_KEYS[el].PARAM} className={styles['radio']}>
                <input
                  type="radio"
                  name="key"
                  value={el}
                  onChange={this.handleType}
                  checked={
                    searchKey
                      ? searchKey.PARAM === SEARCH_KEYS[el].PARAM
                      : false
                  }
                />
                {SEARCH_KEYS[el].DISPLAY}
              </label>
            ))}
          </div>
          <Button
            type="submit"
            text="Submit"
            onClick={this.handleSubmit}
            disabled={!!error}
          />
        </form>
      </div>
    );
  }
}

Search.propTypes = {
  searchKey: SearchType,
  value: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,

  dispatch: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  value: state.search.value,
  error: state.search.error,
  searchKey: state.search.searchKey,
});

export default connect(mapStateToProps)(Search);
