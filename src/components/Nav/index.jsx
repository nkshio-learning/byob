import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './nav.scss';

const Nav = () => (
  <ul className={styles.nav}>
    <li>
      <NavLink exact activeClassName="active" to="/">
        Home
      </NavLink>
    </li>
    <li>
      <NavLink activeClassName="active" to="/discover">
        Discover
      </NavLink>
    </li>
  </ul>
);

export default Nav;
