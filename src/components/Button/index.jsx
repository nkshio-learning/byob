import React from 'react';
import PropTypes from 'prop-types';

import styles from './button.scss';

const Button = props => (
  <button
    type={props.type}
    onClick={props.onClick}
    disabled={!!props.disabled}
    className={props.className ? styles[props.className] : styles['button']}
  >
    {props.text}
  </button>
);

Button.propTypes = {
  text: PropTypes.string,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  type: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Button;
