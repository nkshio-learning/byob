import { schema } from 'normalizr';

export const beer = new schema.Object('beer');
export const arrayOfBeers = new schema.Array(beer);
