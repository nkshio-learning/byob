import { normalize } from 'normalizr';
import * as schema from './schema';
import * as api from '../api';

export const fetchBeer = query => (dispatch, getState) => {
  const state = getState();

  if (state.random.loading) {
    return Promise.resolve();
  }

  dispatch({
    type: 'RANDOM_BEER_REQUEST',
  });

  return api.fetchBeer(query).then(
    response => {
      dispatch({
        type: 'RANDOM_BEER_SUCCESS',
        response:
          response && response[0] ? normalize(response[0], schema.beer) : null,
      });
    },
    error => {
      dispatch({
        type: 'RANDOM_BEER_FAILURE',
        response: error.message,
      });
    },
  );
};

export const discover = (query = {}) => (dispatch, getState) => {
  const state = getState();

  if (
    state.discover.loading ||
    (state.discover.noMoreBeers && query.page > 1)
  ) {
    return Promise.resolve();
  }

  dispatch({
    query,
    type: 'SEARCH_REQUEST',
  });

  return api.discover(query).then(
    response => {
      dispatch({
        query,
        type: 'SEARCH_SUCCESS',
        response: normalize(response, schema.arrayOfBeers),
      });
    },
    error => {
      dispatch({
        type: 'SEARCH_FAILURE',
        response: error.message,
      });
    },
  );
};
