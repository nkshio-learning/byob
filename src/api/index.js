import axios from 'axios';
import dayjs from 'dayjs';

import { PUNK_API } from '../lib/constants';

const axiosWrapper = params =>
  axios
    .get(PUNK_API + params)
    .then(res => res.data)
    .catch(err => {
      throw new Error(err);
    });

export const fetchBeer = (isNonAlcoholic = false) => {
  if (isNonAlcoholic) {
    return axiosWrapper('beers?abv_lt=1');
  }

  return axiosWrapper('beers/random');
};

export const discover = ({ page = 1, searchKey, value }) => {
  let params = `beers?page=${page}`;

  if (searchKey && value) {
    const { TYPE, PARAM } = searchKey;

    if (PARAM && value) {
      if (TYPE === 'date') {
        params += `&${PARAM}=${dayjs(value).format('MM-YYYY')}`;
      } else {
        params += `&${PARAM}=${value}`;
      }
    }
  }

  return axiosWrapper(params);
};
