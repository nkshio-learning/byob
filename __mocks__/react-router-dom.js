import React from 'react';

const Router = require('react-router-dom');

// Just render plain div with its children
Router.NavLink = ({ children }) => <div>{children}</div>;

module.exports = Router;
