# BrewDog Recipes with PunkAPI

This is a simple one-page application that interacts with the PunkAPI. It allows

- Display a random beer section, with a button/link to display random beers and random non-alcoholic beers

- Display a small search form where users can search for beers by name(using a free text search) or `brewed_before` (date)

- Show search results from the [PunkAPI](https://punkapi.com/documentation/v2)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install/run this project

### Commands

- `npm install` - Install dependencies

- `npm test` - Run tests with linting and coverage results.

- `npm run build` - Babel will transpile ES6 => ES5 and minify the code.

### Todo

If this is being deployed to production, following things should enhance the user experience

- [x] A loader for InfiniteScroll in`Discover`

- [x] Optimizations to not load already loaded data

- [ ] Lazy load images

- [ ] i18n/l13n

- [ ] Increase test coverage >90%

- [ ] Render only elementsvisible in the view area with [react-window](https://github.com/bvaughn/react-window)

- [ ] Datepicker element with `Month/Year` format (as PunkAPI supports only `MM-YYYY` format

### Notes

- No effort have been given to the UI enhancements, this app is created only for learning purpose.
- I have not used [create-react-app](https://github.com/facebook/create-react-app) intentionally to get familiar with the build tools. However, I agree this is not the recommended way.
- This app has been tested on Firefox, Chrome only

### Concepts learnt through this exercise

- Setup new project with webpack and essential dependencies
- Presentational Vs HoC components
- PropTypes / Shapes
- State Handling without Redux
- React Router
- State Management with Redux
- Middlewares e.g Thunk, logger
- Snapshot testing with Jest 2

# License

MIT © Ankush Mehta
