import React from 'react';
import renderer from 'react-test-renderer';

import Beer from '../src/components/Beer';

describe('<Beer />', () => {
  describe('full-page', () => {
    it('matches the snapshot', () => {
      let wrapper = renderer.create(
        <Beer
          id={1}
          isFullPage
          name="test-name"
          tagline="test-tagline"
          image_url="test-url"
          description="test-description"
        />,
      );

      expect(wrapper).toMatchSnapshot();
    });

    it('when image-url is null', () => {
      let wrapper = renderer.create(
        <Beer
          id={1}
          isFullPage
          name="test-name"
          tagline="test-tagline"
          description="test-description"
        />,
      );

      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('beer-card', () => {
    it('matches the snapshot', () => {
      let wrapper = renderer.create(
        <Beer
          id={1}
          name="test-name"
          tagline="test-tagline"
          image_url="test-url"
          description="test-description"
        />,
      );

      expect(wrapper).toMatchSnapshot();
    });

    it('when image-url is null', () => {
      let wrapper = renderer.create(
        <Beer
          id={1}
          name="test-name"
          tagline="test-tagline"
          description="test-description"
        />,
      );

      expect(wrapper).toMatchSnapshot();
    });
  });
});
