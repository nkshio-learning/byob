import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import TestUtils from 'react-dom/test-utils';
import configureStore from 'redux-mock-store';

import Search from '../src/components/Search';
import { SEARCH_KEYS } from '../src/lib/constants';

describe('<Search />', () => {
  const initialState = {
    search: {
      value: '',
      error: '',
      searchKey: SEARCH_KEYS[Object.keys(SEARCH_KEYS)[0]],
    },
  };
  const mockStore = configureStore();
  const store = mockStore(initialState);
  const mockCallBack = jest.fn();

  it('matches the snapshot', () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <Search onSubmit={mockCallBack} />
      </Provider>,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('simulate search', () => {
    const view = TestUtils.renderIntoDocument(
      <Provider store={store}>
        <Search onSubmit={mockCallBack} />
      </Provider>,
    );

    const input = TestUtils.scryRenderedDOMComponentsWithTag(view, 'input');

    input.value = 'Black Eyed King Imp';
    TestUtils.Simulate.change(input);

    TestUtils.Simulate.keyDown(input, {
      key: 'Enter',
      keyCode: 13,
      which: 13,
    });

    expect(input.value).toBe('Black Eyed King Imp');
  });
});
