import React from 'react';
import renderer from 'react-test-renderer';

import Nav from '../src/components/Nav';

describe('<Nav />', () => {
  it('matches the snapshot', () => {
    let wrapper = renderer.create(<Nav />);
    expect(wrapper).toMatchSnapshot();
  });
});
