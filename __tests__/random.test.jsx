import React from 'react';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import Random from '../src/components/Random';

describe('<Random />', () => {
  let store;
  let wrapper;
  const middlewares = [thunk];

  const initialState = {
    random: { loading: true, error: '' },
    loading: { text: 'Loading', speed: 300 },
  };
  const mockStore = configureStore(middlewares);

  beforeEach(() => {
    store = mockStore(initialState);
    wrapper = renderer.create(
      <Provider store={store}>
        <Random />
      </Provider>,
    );
  });

  it('matches the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
