import React from 'react';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import List from '../src/components/List';

describe('<List />', () => {
  const initialState = {
    loading: {
      text: 'Loading',
      speed: 300,
    },
  };
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);
  const store = mockStore(initialState);

  const mockCallBack = jest.fn();
  let wrapper = renderer.create(
    <Provider store={store}>
      <List
        loading
        beers={[
          {
            id: 1,
            name: 'test',
            tagline: 'test',
            image_url: 'test',
            description: 'test',
          },
        ]}
        onInfinite={mockCallBack}
      />
    </Provider>,
  );

  it('matches the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
