import React from 'react';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import Loading from '../src/components/Loading';

describe('<Loading />', () => {
  const initialState = {
    loading: {
      text: 'Loading',
      speed: 300,
    },
  };
  const middlewares = [thunk];
  const mockStore = configureStore(middlewares);
  const store = mockStore(initialState);

  it('matches the snapshot', () => {
    let wrapper = renderer.create(
      <Provider store={store}>
        <Loading />
      </Provider>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});
