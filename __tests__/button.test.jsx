import React from 'react';
import renderer from 'react-test-renderer';

import Button from '../src/components/Button';

describe('<Button />', () => {
  const mockCallBack = jest.fn();
  let wrapper = renderer.create(
    <Button type="button" text="Random Beer" onClick={mockCallBack} />,
  );

  it('matches the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
