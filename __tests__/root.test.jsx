import React from 'react';
import thunk from 'redux-thunk';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import Root from '../src/components/Root';

describe('<Root />', () => {
  let store;
  let wrapper;
  const middlewares = [thunk];

  const initialState = {
    random: { loading: true, error: '' },
    loading: { text: 'Loading', speed: 300 },
  };
  const mockStore = configureStore(middlewares);

  beforeEach(() => {
    store = mockStore(initialState);
    wrapper = renderer.create(<Root store={store} />);
  });

  it('matches the snapshot', () => {
    expect(wrapper.toJSON()).toMatchSnapshot();
  });
});
