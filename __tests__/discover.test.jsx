import React from 'react';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import Discover from '../src/components/Discover';
import { SEARCH_KEYS } from '../src/lib/constants';

describe('<Discover />', () => {
  let store;
  let wrapper;
  const middlewares = [thunk];

  const initialState = {
    discover: {
      error: '',
      beers: [],
      query: {
        page: 1,
      },
      loading: false,
      noMoreBeers: false,
    },
    search: {
      value: '',
      error: '',
      type: SEARCH_KEYS[Object.keys(SEARCH_KEYS)[0]],
    },
  };
  const mockStore = configureStore(middlewares);

  beforeEach(() => {
    store = mockStore(initialState);
    wrapper = renderer.create(
      <Provider store={store}>
        <Discover />
      </Provider>,
    );
  });

  it('matches the snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
